from configparser import ConfigParser
import os
from collections import OrderedDict


class ConfigManager:
    _configs = {}

    def __init__(self, app_config):
        # Verify the configuration path
        if self.validate(app_config):
            # Gets all configuration files in the directory by calling get_config_file
            app_config_file_list = self.get_config_file(app_config)
            # Get configuration data
            ConfigManager._configs = self.__parse_config(app_config_file_list)
        print(ConfigManager._configs)

    def validate(self, app_config):
        if not os.path.exists(app_config):
            raise FileNotFoundError("No such file or directory {}".format(app_config))
        if not os.listdir(app_config):
            return False
        return True

    def get_config_file(self, app_config):
        file_list = []
        for file in os.listdir(app_config):
            if os.path.splitext(file)[-1] != ".ini":
                continue
            real_file = os.path.join(app_config, file)
            file_list.append(real_file)
        return file_list

    def __parse_config(self, app_config_file_list):
        config_dict = {}
        for app_config_file in app_config_file_list:
            cfg = ConfigParser()
            cfg.read(app_config_file)
            sections = cfg.sections()
            for section in sections:
                section_dict = {}
                for key, value in cfg.items(section):
                    # Determine whether there are variables
                    if "$" in key:
                        variant_list = key.split("$")
                        variant = variant_list[0]
                        real_key = variant_list[1]
                        # judge the key is already in section dict or not
                        if real_key in section_dict.keys():
                            section_dict[real_key]["priority"][variant] = value
                        # a new key,should create a new value dict for the key
                        else:
                            value_dict = {
                                "priority": OrderedDict(),
                                "default": None
                            }
                            value_dict["priority"][variant] = value
                            section_dict[real_key] = value_dict
                    # just simple key value config
                    else:
                        if key in section_dict.keys():
                            section_dict[key]["default"] = value
                        else:
                            value_dict = {
                                "priority": OrderedDict(),
                                "default": value
                            }
                            section_dict[key] = value_dict
                if section in config_dict.keys():
                    for section_key, section_value in section_dict.items():
                        config_dict[section][section_key] = section_value
                else:
                    config_dict[section] = section_dict
        return config_dict

    def get_string_value_by_key(self, section: str, key: str, default=None, variants=None):
        if type(section) != str:
            raise KeyError("The {} must be <class 'str'>".format(section))
        if type(key) != str:
            raise KeyError("The {} must be <class 'str'>".format(key))
        if variants is None:
            if section in ConfigManager._configs.keys():
                if key in ConfigManager._configs[section].keys():
                    value = ConfigManager._configs[section][key]["default"]
                    return value
            return default
        else:
            if type(variants) != list:
                raise KeyError("the {} must be <class 'list'>".format(variants))
            if section in ConfigManager._configs.keys():
                if key in ConfigManager._configs[section].keys():
                    for variant in variants:
                        if type(variant) != dict:
                            raise KeyError("the elements of {} must be <class 'dict'>".format(variants))
                        var_type, var_value = variant.values()
                        var_type_value = "@".join([var_type.lower(), var_value.lower()])
                        for type_value, value in ConfigManager._configs[section][key]["priority"].items():
                            if var_type_value == type_value:
                                return value
                    return ConfigManager._configs[section][key]["default"]
            return default

    def get_int_value_by_key(self, section: str, key: str, default=None, variants=None):
        value = self.get_string_value_by_key(section, key, variants)
        if value is None:
            return default
        try:
            int_value = int(value)
        except:
            raise Exception("The value maybe <class 'str'> not <class 'int'>")
        return int_value

    def get_float_value_by_key(self, section: str, key: str, default=None, variants=None):
        value = self.get_string_value_by_key(section, key, variants)
        if value is None:
            return default
        try:
            float_value = float(value)
        except:
            raise Exception("The value maybe <class 'str'> not <class 'float'>")
        return float_value

    def get_bool_value_by_key(self, section: str, key: str, default=None, variants=None):
        value = self.get_string_value_by_key(section, key, variants)
        if value is None:
            return default
        if value.lower() == "true":
            return True
        elif value.lower() == "false":
            return False
        else:
            raise Exception("the value maybe <class 'str'> not <class 'bool'>")

    def get_list_value_by_key(self,section: str, key: str, default=None, variants=None):
        value = self.get_string_value_by_key(section, key, variants)
        if value is None:
            return default
        value_list = value.split(",")
        return value_list
